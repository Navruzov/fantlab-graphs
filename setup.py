# Copyright (c) 2020, Fedor Navruzov
# All rights reserved.

# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree.

import setuptools
from os.path import join as pjoin

REQUIREMENTS = [
    'requirements.txt'
]

PACKAGE_NAME = 'fantlabgraph'

# Get global version
# see: https://packaging.python.org/guides/single-sourcing-package-version/
version = {}
with open(pjoin(PACKAGE_NAME, "version.py"), "r") as fh:
    exec(fh.read(), version)
VERSION = version["__version__"]


def parse_requirements(file):
    lines = (line.strip() for line in open(file))
    return [
        line for line in lines
        if line and not (line.startswith("#") or line.startswith('-'))
    ]


def get_all_requirements():
    requirements = []
    for path in REQUIREMENTS:
        requirements += parse_requirements(path)
    return requirements


setuptools.setup(
    name=PACKAGE_NAME,
    version=VERSION,
    author='Fedor Navruzov',
    author_email='fred.navruzov@gmail.com',
    description='Library for graph-based stuff over fantasy books',
    # url='#TODO',
    packages=setuptools.find_packages(),
    install_requires=get_all_requirements(),
    license="BSD 3-Clause",
    python_requires='>=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: Apache Software License",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
    ]
)
