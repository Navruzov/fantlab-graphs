# Notebooks
---
**Here are the demo `Jupyter` notebooks regarding**
- data parsing
- data preprocessing
- database creation (`neo4j`)
- model training
- visualizations
- etc.