import os
import random
import re
import shutil
import time
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor, as_completed
from os import cpu_count
from os.path import join as pjoin
from typing import Callable, Iterable, Sized, Union

import numpy as np
import pandas as pd
import requests
import ujson
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from joblib import Parallel, delayed
from requests import HTTPError
from tqdm.auto import tqdm
from deprecated.sphinx import deprecated


# data directory constants
FANTLAB_API = 'https://api.fantlab.ru'
DATA_DIR = pjoin('..', 'data')
AUTHOR_DIR = pjoin(DATA_DIR, 'authors')
AUTHOR_IMG_DIR = pjoin(AUTHOR_DIR, 'images')
AUTHOR_INFO_DIR = pjoin(AUTHOR_DIR, 'info')
USER_DIR = pjoin('..', 'data', 'users')
USER_INFO_DIR = pjoin(USER_DIR, 'info')


def save_json(json, fname: str, mode: str = 'w+') -> None:
    """
    Save selected JSON to local filesystem

    Parameters
    ----------
    json: JSON-compatible object
    fname: str

    mode: str, default='w'
        File mode to open with, defaults to 'w'
        May be 'wb', etc.

    Returns
    -------
    None
    """
    with open(fname, mode=mode) as out_file:
        ujson.dump(json, out_file)


def run_parallel_save_async(func: Callable, elements: Union[Iterable, Sized], max_workers: int = 2,
                            use_multiprocessing: bool = False, max_runs: int = 5,
                            random_order: bool = True, verbose: bool = False, **kwargs) -> None:
    """
    Run save jobs in asynchronous way given the save `func`.
    Recursively resubmit failed jobs up to `max_runs` times.
    Shows progress with `tqdm`

    Parameters
    ----------
    func: Callable
        Function to call while saving.
        Must have positional required argument `element_id`

    elements: Iterable
        The collection of elements to call `func` for

    max_workers: int, default 2
        Number of jobs to use
        (threads if `use_multiprocessing` == False or CPUs if True)

    use_multiprocessing: bool, default False
        Whether to use `concurrent.futures.ThreadPoolExecutor` (False)
        or `concurrent.futures.ProcessPoolExecutor` (True)
        for jobs execution

    max_runs: int, default 5
        How many times recursively resubmit failed jobs (if any)
        until breaking

    random_order: bool, default True
        Whether to shuffle elements before processing them

    verbose: bool, default False
        Whether to print warnings / exceptions etc.

    kwargs: dict
        `func` kwargs

    Returns
    -------
    None
    """

    if max_runs == 0:
        print('max runs exceeded, stopping execution...')
        return

    if random_order:
        np.random.shuffle(elements)

    if use_multiprocessing:
        # adjust workers to CPU count
        max_workers = min(max_workers, cpu_count())
        executor = ProcessPoolExecutor(max_workers=max_workers)
    else:
        executor = ThreadPoolExecutor(max_workers=max_workers)

    # We can use a with statement to ensure threads are cleaned up promptly
    failed_tasks = []
    with executor:
        # wrap with tqdm to track progress of task completion
        with tqdm(total=len(elements), desc='processing elements...') as progress:
            # Start the load operations and mark each future with its URL

            futures = dict()
            for el in elements:
                future = executor.submit(func, el, **kwargs)
                future.add_done_callback(lambda p: progress.update())
                futures[future] = el

            processed = []
            for future in as_completed(futures):
                try:
                    future.result()
                    processed.append(futures[future])

                except Exception as e:
                    if verbose:
                        print(f'Got exception: {e}, resubmit...')
                    failed_tasks.append(futures[future])

            processed = list(set(processed))
            assert len(processed) + len(failed_tasks) == len(elements), \
                f'Looks like {len(elements) - len(processed) - len(failed_tasks)} tasks got lost...'

    if failed_tasks:
        print(f'N of failed tasks: {len(failed_tasks)}')
        # recursively resubmit and execute failed jobs
        run_parallel_save_async(
            func=func,
            elements=list(set(failed_tasks)),
            max_workers=max_workers,
            use_multiprocessing=use_multiprocessing,
            max_runs=max_runs - 1,
            **kwargs
        )


@deprecated(
    version='0.0.2',
    reason="You should use another `fantlabgraph.scraping.run_parallel_save_async` instead"
)
def run_parallel_save_func(
        func: Callable,
        elements: Iterable,
        dirname: str,
        obj_type: str = 'user',
        n_jobs: int = None,
        backend: str = 'threading',
        random_order: bool = False,
        **func_params,
):
    """
    Deprecated function for saving.
    Use `run_save_func_async` instead
    """
    if n_jobs is None:
        n_jobs = cpu_count()
    if random_order:
        np.random.shuffle(elements)
    try:
        Parallel(n_jobs=n_jobs, backend='threading')(
            delayed(func)(element_id=element, dirname=dirname, **func_params)
            for element in tqdm(elements, desc=f'scraping {obj_type} list...')
        )
        pass
    except (
            requests.exceptions.SSLError,
            requests.exceptions.ConnectionError,
    ):
        time.sleep(max(5, np.random.rand() * 10))
        run_parallel_save_func(
            func=func,
            elements=elements,
            dirname=dirname,
            obj_type=obj_type,
            n_jobs=n_jobs,
            backend=backend,
            **func_params,
        )


def get_proxy_df() -> pd.DataFrame:
    """
    Getting free HTTPs proxy list

    Returns
    -------
    proxies : pd.DataFrame
        DataFrame with proxies
    """
    proxies = requests.get('https://free-proxy-list.net/')
    proxies = BeautifulSoup(proxies.text, features="lxml")
    proxies = pd.read_html(
        str(proxies.find('table', attrs={'id': 'proxylisttable'})))[0]
    proxies = proxies[proxies['Port'].notnull() & (proxies.Https == 'yes')]
    proxies['Port'] = proxies['Port'].astype(int)
    proxies.columns = [c.lower().replace(' ', '_') for c in proxies.columns]
    return proxies


def get_random_proxy() -> str:
    """
    Get random proxy
    Warnings!
    Free use is constrained to 50 calls per 24h or so

    Returns
    -------
    proxy : str
        Proxy string of form 'IP:PORT'
    """
    proxy_url = 'http://pubproxy.com/api/proxy?limit=5&format=txt&https=true&type=https&speed=10'
    page = requests.get(proxy_url)
    return random.choice(page.text.split('\n'))


def get_page(page_url: str, retries: int = 0, max_retries: int = 5,
             **request_kwargs) -> requests.models.Response:
    """
    Get selected URL content given params

    Parameters
    ----------
    page_url: str
        Page to get content from

    retries: int, default = 5
        How many page retries have already been done
        Starts from 0, for recursive calls

    max_retries: int, default = 25
        How many retries to make until give up
        If > 0 continues recursively call itself

    request_kwargs: dict
        Dict of params for `request.get` func

    Returns
    -------
    result: requests.models.Response
        Response body
    """
    time.sleep(max(0.2, np.random.rand() * 1.5))

    if retries > max_retries:
        raise requests.exceptions.BaseHTTPError(
            'too many 429 errors handled, breaking'
        )

    headers = request_kwargs.get('headers', {'User-Agent': UserAgent().random})

    try:
        stream = request_kwargs.pop('stream')
    except KeyError:
        stream = False

    response = requests.get(
        page_url,
        stream=stream,
        headers=headers,
        timeout=60,
        **request_kwargs
    )
    if response.ok:
        return response
    elif response.status_code == 404:
        raise HTTPError(
            f'404: page {page_url} doesn\'t exist')
    # handle too many requests
    elif response.status_code == 429:
        retries += 1
        time.sleep(max(2, np.random.rand() * 4))
        return get_page(
            page_url=page_url,
            retries=retries,
            max_retries=max_retries,
            **request_kwargs
        )

    else:
        raise HTTPError(
            f'Got HTTP error: {response.status_code} on page {page_url}')


def get_all_authors() -> dict:
    """
    API spec
    {
        list:[
            {
                type: String           # тип сегмента (в данном случае всегда autor)
                id: Int,               # id автора
                url: Url               # ссылка на страницу автора
                autor_id: Int,         # id автора
                is_fv: Boolean,        # является ли автор фантастоведом
                name: String,          # имя на русском языке
                name_orig: String,     # имя на языке оригинала
                name_rp: String,       # имя на русском языке в родительном падеже
                name_short: String,    # имя на русском языке для перечислений (сначала фамилия, затем имя)
            },
            ...
        ],
        liter: String,                 # по какой заглавной букве фамилии ищем
        liter_code: Int|null,          # код заглавной буквы фамилии (в кодировке Windows-1251)
        liter_list: String             # список ссылок на побуквенные списки авторов
    }
    """
    authors_list_link = FANTLAB_API + '/autorsall'
    response = ujson.loads(get_page(authors_list_link).text)
    return {e['id']: e for e in response['list']}


def save_author_info(element_id: int, dirname: str,
                     extended: bool = False, save_full_img: bool = True) -> None:
    """
    {
        type: String                      # тип сегмента (в данном случае всегда autor)
        id: Int,                          # id автора
        autor_id: Int,                    # id автора (дубль, название переменной с типом)
        url: Url                          # ссылка на страницу автора
        last_modified: DateTime,          # дата последнего редактирования

        is_opened: Boolean,               # открыта ли страница автора
        anons: String,                    # краткий анонс биографии
        birthday: Date|null,              # дата рождения
        country_id: Int|null,             # id страны
        country_name: String,             # название страны
        deathday: Date|null,              # дата смерти
        fantastic: Int,                   # ?
        image: String,                    # ссылка на основное фото автора
        image_preview: String,            # ссылка на превью основного фото автора
        name: String,                     # имя на русском языке
        name_orig: String,                # имя в оригинале
        name_pseudonyms: [                # список псевдонимов
            ...: String|null,
            ...
        ],
        name_rp: String,                  # имя на русском языке в родительном падеже
        name_short: String,               # имя на русском языке для перечислений (сначала фамилия, затем имя)
        sex: String,                      # пол ("m" - мужской, "f" - женский)
        stat: {                           # статистика
            awardcount: Int|null,         # [any] количество наград
            editioncount: Int,            # количество изданий
            markcount: Int,               # количество поставленных автору оценок
            moviecount: Int,              # количество фильмов (экранизаций и т.д.)
            responsecount: Int,           # количество написанных на произведения автора отзывов
            workcount: Int|null           # [any] количество произведений
        },
        sites: [ |null                    # [any] сайты автора
            {
                descr: String,            # описание ссылки
                site: String              # ссылка
            },
            ...
        ],
    }

    в расширенной информации о произведении добавляются блоки:
    biography - биография
    classificatory - жанровая классификация
    analogs - подборка аналогичных авторов (по классификации)
    awards - награды
    la_resume - лингвоанализ
    biblio_blocks - библиография (список произведений)
    sort - вариант сортировки библиографии (необязательный)
    / year - по году публикации
    / rating - по рейтингу
    / markcount - по количеству оценок
    / rusname - по русскому названию
    / name - по оригинальному названию
    / writeyear - по году написания
    """
    author_info_url = (
            FANTLAB_API + (f'/autor/{element_id}/extended'
                           if extended else f'/autor/{element_id}')
    )

    dirname_info = pjoin(dirname, 'info')
    dirname_img = pjoin(dirname, 'images')

    fname = pjoin(dirname_info, str(element_id) + '.json')
    if not os.path.exists(dirname_info):
        os.makedirs(dirname_info, exist_ok=True)
    if not os.path.exists(dirname_img):
        os.makedirs(dirname_img, exist_ok=True)

    if not os.path.exists(fname):
        author_data = ujson.loads(get_page(author_info_url).text)
        author_data['id'] = int(author_data['id'])
        for k in ['anons', 'biography']:
            try:
                # strip non-breaking spaces, etc.
                author_data[k] = re.sub(r'\s+', ' ', author_data[k])
            except KeyError:
                pass
        # save info
        save_json(author_data, fname)
    # save img
    save_author_image(element_id, dirname, small=not save_full_img)


def save_author_image(author_id: int, dirname: str, small: bool = False) -> None:
    """
    Save author image data

    Parameters
    ----------
    author_id: int
        Author's id to scrap image for

    dirname: str
        Where to save obtained image

    small: bool, default = False
        Whether to download thumbnail (True)
        or full picture (False)
    Returns
    -------
    None
    """
    dirname = pjoin(dirname, 'images')

    def _save_image_to_file(image, filename):
        with open(filename, 'wb') as out_file:
            shutil.copyfileobj(image.raw, out_file)

    img_url = f'https://data.fantlab.ru/images/autors/{"small/" if small else ""}{author_id}'

    fname = pjoin(dirname, str(author_id) +
                  ('_preview' if small else '') + '.jpg')

    fname_dir = os.path.split(fname)[0]
    if not os.path.exists(fname_dir):
        os.makedirs(fname_dir)

    if not os.path.exists(fname):
        time.sleep(max(0.2, np.random.rand() / 2))
        response = get_page(img_url, **{'stream': True})
        _save_image_to_file(response, fname)


def get_user_page_url(page_n: int = 1) -> str:
    return f'https://fantlab.ru/usersclasspage{page_n}'


def get_user_list_from_page(page_url: str) -> None:
    """
    Gets list of user ids given user page URL
    and save it to user's data dir

    Parameters
    ----------
    page_url: str
        URL to get user list from

    Returns
    -------
    None
    """
    page_number = re.findall('[0-9]+', page_url)[0]
    fname = pjoin(USER_DIR, f'users_page_{page_number}') + '.json'

    if not os.path.exists(fname):
        time.sleep(max(1, np.random.rand() * 3))
        page = get_page(page_url)
        soup = BeautifulSoup(page.text, features="lxml")

        users = [
            int(re.findall('[0-9]+', a.attrs['href'])[0])
            for td in soup.findAll('td', attrs={'colspan': '2'})
            for a in td.findAll('a')
            if re.match(r'^.*user[0-9]+$', a.attrs.get('href', ''))
        ]

        save_json(users, fname=fname)


def get_user_pages_cnt() -> int:
    """
    Get all user pages
    (as it's currently unavailable through public API)

    Returns
    -------
    page_qty: int
        Number of user pages on site
    """
    first_user_page = get_user_page_url(page_n=1)
    page = get_page(first_user_page)
    soup = BeautifulSoup(page.text, features="lxml")
    page_qty = int(
        soup.find(
            'td',
            attrs={'class': 'block-on-mobile'}
        ).findAll('a')[-1].text
    )
    return page_qty


def save_user_pages(pages) -> None:
    """
    Save user info
    given pages of user ids

    Parameters
    ----------
    pages: Iterable

    Returns
    -------
    None
    """
    np.random.shuffle(pages)
    try:
        Parallel(n_jobs=cpu_count(), backend='threading')(
            delayed(get_user_list_from_page)(get_user_page_url(i))
            for i in tqdm(pages, desc='scraping user list...')
        )
    except (
            requests.exceptions.SSLError,
            requests.exceptions.ConnectionError,
    ):
        time.sleep(max(1, np.random.rand() * 10))
        save_user_pages(pages)
