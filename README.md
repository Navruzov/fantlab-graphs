# Fantasy books similarity
---
This repository aims to apply graph-based neural networks for many downstream tasks:
- recommender systems (user similarity, book/author similarity,     etc.)
- attribute restoration, like book's genre tree (link prediction) 
- temporal evolution (user preferences, author popularity, etc.)

The results are based on data, parsed from [fantlab.ru](https://fantlab.ru)
<br>through their publicly available [API](https://api.fantlab.ru/)
<br>(please find the corresponding docs [here](https://github.com/FantLab/FantLab-API))

**Disclaimer**
<br>All rights belong to their respective owners. 
<br>I do not own any of the data content available through the API

P.s.
<br>Be gentle when using their API, do not make servers down :)
<br>The repository usage is **for your own responsibility**